// https://www.drupal.org/docs/7/api/javascript-api/ajax-in-drupal-using-jquery
(function($) {
    Drupal.behaviors.myModule = {
        'attach': function(context) {
            $('a.payment_subscriptions-subscriber:not(.payment_subscriptions-subscriber-processed)', context)
                    .addClass('payment_subscriptions-subscriber-processed')
                    .bind('click', function() {
                $.get(Drupal.settings.basePath + '/payment_subscriptions/subscriber/' + parseInt(this.id), null, tableDetails);
                location.reload();
                return false;
            });
            $('.payment_subscriptions-status:not(.payment_subscriptions-status-processed)', context)
                    .addClass('.payment_subscriptions-status-processed')
                    .bind('change', function() {
                var selected_value = this.value;
                var uid = $(this).attr('uid');
                var evid = $(this).attr('nid');
                $.get(Drupal.settings.basePath + '/payment_subscriptions/status/' + parseInt(evid) + '/' + parseInt(uid) + '/' + parseInt(selected_value), null, tableDetails);
                location.reload();
                return false;
            });
        }
    }

    var tableDetails = function(response) {
        $('.payment_subscriptions-table').html(response);
    }
})(jQuery);

It creates a module to subscribe to paid events. 
a) the user has to charge his account with money from some source like Paypal.
b) the user subscribes to some event where he wants to participate. Then, a certain amount of money is discounted from its account.  
c) The author of the event has to authorise the subscription request. This user can "accept" or "reject" the request. The default status is "pending".
